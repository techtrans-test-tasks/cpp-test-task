#include <chrono>

#include "ParserThread.h"
#include "WriterThread.h"

int main()
{
    using namespace std::chrono_literals;

    ParserThread parser;
    WriterThread writer;

    parser.Run();
    writer.Run();

    unsigned int steps = 100;
    while (steps--)
    {
        parser.GetParseDataEvent()->SetEvent();
        if (steps % 2 == 0)
            writer.GetWriteDataEvent()->SetEvent();
        std::this_thread::sleep_for(1s);
    }
    std::cout << std::endl;
    writer.Stop();
    parser.Stop();
}
