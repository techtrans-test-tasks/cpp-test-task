#pragma once

#include <cstring>
#include <fstream>
#include <iostream>
#include <iterator>
#include <queue>
#include <thread>

#include "EventContainer.h"
#include "Message.h"
#include "constants.h"
#include "crc_16.h"

enum class MsgState
{
    UNDEF,
    START,
    DATA,
    CRC
};

class ParserThread : public EventContainer
{

    std::thread th;

    std::deque<unsigned char> inputData;

    std::shared_ptr<TriggerEvent> StopEvent = std::make_shared<TriggerEvent>(mut, conVar);
    std::shared_ptr<TriggerEvent> ParseDataEvent = std::make_shared<TriggerEvent>(mut, conVar);

    MsgState state = MsgState::UNDEF;
    unsigned char messageLength;

    std::vector<unsigned char> currentMessage;

    static void ThreadStart(void* param);
    void Thread();

    void SaveData();
    void CheckStartMark();
    void SetLength();
    bool CheckCRC();
    void FormMessage();

public:
    ~ParserThread(void);
    void Run();
    void Stop();
    std::shared_ptr<TriggerEvent> GetParseDataEvent();
};
