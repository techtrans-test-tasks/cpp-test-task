/*! \file crc_16.h */
#pragma once


//-------------------------------------------------//
//   Name   : "CRC-16-CCITT"                       //
//   Width  : 16                                   //
//   Poly   : 0x1021                               //
//   Init   : 0xffff                               //
//   RefIn  : false                                //
//   RefOut : false                                //
//   XorOut : 0x0                                  //
//   Check  : 0x29B1//for ASCII string "123456789" //
//-------------------------------------------------//
//   fast lookup table algorithm                   //
//   without augmented zero bytes                  //
//-------------------------------------------------//

unsigned short crc_16 (unsigned char *p, unsigned short len, unsigned short crc = 0xffff);
