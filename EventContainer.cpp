#include "EventContainer.h"

// Функция проверки наличия сработавшего события
bool EventContainer::CheckEvents(std::vector<std::shared_ptr<TriggerEvent>>& list)
{
	bool activeEvent = false;
	for (auto TriggerEventIter = list.begin(); TriggerEventIter != list.end(); TriggerEventIter++)
	{
		if ((*TriggerEventIter)->CheckEvent())
		{
			activeEvent = true;
		}
	}
	return activeEvent;
}

// Функция получения номера сработавшего события. Если таких нет - возвращает UINT_MAX
unsigned int EventContainer::GetActiveEventNum(std::vector<std::shared_ptr<TriggerEvent>>& list)
{
	for (auto TriggerEventIter = list.begin(); TriggerEventIter != list.end(); TriggerEventIter++)
	{
		if ((*TriggerEventIter)->CheckEvent())
			return std::distance(list.begin(), TriggerEventIter);
	}
	return std::numeric_limits<unsigned int>::max();
}

// Функция ожидает следующее событие и возвращает номер сработающего события
unsigned int EventContainer::WaitForEvents(std::vector<std::shared_ptr<TriggerEvent>>& list)
{
	std::unique_lock<std::mutex> lk(mut);
	conVar.wait(lk, [this, &list]() {
		return CheckEvents(list);
		});
	unsigned int EventNum = GetActiveEventNum(list);
	list.at(EventNum)->ResetEvent();
	return EventNum;
}
