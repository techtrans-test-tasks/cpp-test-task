#pragma once
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <vector>

// Класс описывающий срабатывающие события. Хранит ссылки на мютекс и condition_variable
// связанные с ожидающим события тредом, а также признак срабатывания.
class TriggerEvent
{
	std::atomic_bool Event{false};
	std::mutex& mut;
	std::condition_variable& conVar;
public:
	TriggerEvent(std::mutex& m, std::condition_variable& c): mut(m), conVar(c) {};
	void ResetEvent() // Сброс события после обработки
	{
		Event = false;
	}
	bool CheckEvent() { return Event; };
	void SetEvent() // Активация события
	{
		std::unique_lock<std::mutex> lk(mut);
		Event = true;
		conVar.notify_all();
	}
};

// Класс, от которого наследуются классы, реализующие методы ожидания событий
class EventContainer
{
protected:
	std::mutex mut;
	std::condition_variable conVar;

	unsigned int WaitForEvents(std::vector<std::shared_ptr<TriggerEvent>>& EventList);

	bool CheckEvents(std::vector<std::shared_ptr<TriggerEvent>>& EventList);
	unsigned int GetActiveEventNum(std::vector<std::shared_ptr<TriggerEvent>>& EventList);
};
