#pragma once

#include "constants.h"

struct Message
{
    unsigned char size;
    unsigned char id;
    unsigned char data[MAX_SIZE - 4]; // SIZE(1), ID(1), CRC(2) не записаны в данные
};
