#pragma once

#include <cstring>
#include <fstream>
#include <iostream>
#include <list>
#include <thread>

#include "EventContainer.h"
#include "Message.h"
#include "constants.h"

class WriterThread : public EventContainer
{

    unsigned char currentId = 0;
    std::thread th;

    std::list<std::shared_ptr<Message>> messageList;

    std::shared_ptr<TriggerEvent> StopEvent = std::make_shared<TriggerEvent>(mut, conVar);
    std::shared_ptr<TriggerEvent> WriteDataEvent = std::make_shared<TriggerEvent>(mut, conVar);

    std::vector<unsigned char> currentMessage;

    static void ThreadStart(void* param);
    void Thread();

public:
    ~WriterThread(void);
    void Run();
    void Stop();
    std::shared_ptr<TriggerEvent> GetWriteDataEvent();
    void WriteMessage(Message & msg);
};
