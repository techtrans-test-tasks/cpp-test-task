#include "ParserThread.h"

ParserThread::~ParserThread(void)
{
    if (th.joinable())
        th.join();
}

void ParserThread::Run()
{
    std::ifstream inFile("in.dat", std::ios_base::binary);

    inFile.unsetf(std::ios::skipws);

    std::copy(
        std::istream_iterator<unsigned char>(inFile),
        std::istream_iterator<unsigned char>(),
        std::back_inserter(inputData));

    th = std::thread(ThreadStart, this);
}

void ParserThread::ThreadStart(void* lpParam)
{
    try
    {
        ((ParserThread*)lpParam)->Thread();
    }
    catch (...)
    {
        std::cerr << "Error in ParserThread" << std::endl;
        exit(0);
    }
}

void ParserThread::Thread()
{
    std::vector<std::shared_ptr<TriggerEvent>> EventList;
    EventList.push_back(ParseDataEvent); // Event 0
    EventList.push_back(StopEvent); // Event 1

    while (1)
    {
        unsigned int eventNum = WaitForEvents(EventList);
        std::cout << ">" << std::flush;
        bool messageAdded = false;
        switch (eventNum)
        {
            case 0: // ParseDataEvent
                while (!inputData.empty() && !messageAdded)
                {
                    switch (state)
                    {
                        case MsgState::UNDEF:
                            CheckStartMark();
                            break;
                        case MsgState::START:
                            SetLength();
                            break;
                        case MsgState::DATA:
                            SaveData();
                        break;
                        case MsgState::CRC:
                            messageAdded = CheckCRC();
                            if (messageAdded)
                                FormMessage();
                        break;
                    }
                }
                break;
            case 1: //StopEvent
                return;
        }
    }
}

void ParserThread::Stop()
{
    StopEvent->SetEvent();
}

std::shared_ptr<TriggerEvent> ParserThread::GetParseDataEvent()
{
    return ParseDataEvent;
}

void ParserThread::CheckStartMark()
{
    if (inputData.front() == START_BYTE)
        state = MsgState::START;
    inputData.pop_front();
}

void ParserThread::SetLength()
{
    messageLength = inputData.front();
    if (messageLength <= MAX_SIZE)
        currentMessage.push_back(messageLength), state = MsgState::DATA;
    else
        state = MsgState::UNDEF;
    inputData.pop_front();
}

void ParserThread::SaveData()
{
    currentMessage.push_back(inputData.front());
    if (currentMessage.size() == messageLength)
        state = MsgState::CRC;
    inputData.pop_front();
}

bool ParserThread::CheckCRC()
{
    bool retValue = false;
    unsigned short crc16 = (*(currentMessage.end() - 1) << 8) | *(currentMessage.end() - 2);
    if (crc16 == crc_16(currentMessage.data(), currentMessage.size() - 2))
    {
        retValue = true;
    }
    currentMessage.clear();
    state = MsgState::UNDEF;
    return retValue;
}

void ParserThread::FormMessage()
{
    std::shared_ptr<Message> msg = std::make_shared<Message>();
    msg->size = messageLength - 4;
    msg->id = currentMessage.at(0);
    std::memcpy(msg->data, currentMessage.data() + 2, msg->size);
}
