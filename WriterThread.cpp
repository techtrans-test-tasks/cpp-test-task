#include "WriterThread.h"

WriterThread::~WriterThread(void)
{
    if (th.joinable())
        th.join();
}

void WriterThread::Run()
{
    std::ofstream outFile("out.txt");

    th = std::thread(ThreadStart, this);
}

void WriterThread::ThreadStart(void* lpParam)
{
    try
    {
        ((WriterThread*)lpParam)->Thread();
    }
    catch(...)
    {
        std::cerr << "Error in ParserThread" << std::endl;
        exit(0);
    }
}

void WriterThread::Thread()
{
    std::vector<std::shared_ptr<TriggerEvent>> EventList;
    EventList.push_back(WriteDataEvent); // Event 0
    EventList.push_back(StopEvent); // Event 1

    while(1)
    {
        unsigned int eventNum = WaitForEvents(EventList);
        std::cout << "+" << std::flush;
        switch (eventNum)
        {
            case 0: //WriteDataEvent
                if (!messageList.empty())
                {
                    for (auto it = messageList.begin(); it != messageList.end(); ++it)
                    {
                        if ((**it).id == currentId)
                        {
                            WriteMessage(**it);
                            currentId++;
                            messageList.erase(it);
                            break;
                        }
                    }
                }
                break;

            case 1: //StopEvent
                return;
        }
    }
}

void WriterThread::Stop()
{
    StopEvent->SetEvent();
}

std::shared_ptr<TriggerEvent> WriterThread::GetWriteDataEvent()
{
    return WriteDataEvent;
}

void WriterThread::WriteMessage(Message& msg)
{
    std::ofstream outFile("out.txt", std::ios_base::app);
    for (unsigned int i = 0; i < msg.size; i++)
    {
        outFile << (unsigned char)~(msg.data[i]);
    }
}
